$Servers = Get-Content -Path C:\ServerNames.txt 
$Username = "Domain\Enter Your User Name Here"
$Password = ConvertTo-SecureString -String "Enter Your Password Here" -AsPlainText -Force
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username, $Password
$Date = Get-Date -Format dd-MM-yyyy
 
#Reboots the given set of servers
Foreach ($Server in $Servers){
    try {
        $Message = New-Object -ComObject Wscript.Shell
        $Message.Popup("This system is going to reboot now!", 10, "Please save your files and data", 0x0)
        $Message.Popup("60 seconds to reboot", 2, "Save your files", 0x0)
        $xCmdString = { sleep 60 }
        Invoke-Command $xCmdString
        Write-Host "Restarting $Server"
        "Server $Server initiated reboot at $(Get-Date)" | Add-Content -Path C:\RebootServerLog_$Date.txt
        Restart-Computer -ComputerName $Server -Credential $Credential -Force
    }
    catch [System.Exception]{
        Write-Host "Failed to restart `n$error[0]"        
    }
}
 
# Checks the status of the server
forEach ($Server in $Servers)
{
    if (Test-Connection $Server -quiet){ 
        "Server $Server reboot has completed at $(Get-Date)" | Add-Content -Path C:\RebootedServerLog_$Date.txt 
    }
    else{ 
        "Destination $Server unreachable at $(Get-Date)" | Add-Content -Path C:\RebootedServerLog_$Date.txt 
    }
}
